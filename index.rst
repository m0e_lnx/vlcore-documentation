
###########################
Welcome to |vector_edition|
###########################


Introduction
============

Speed, performance, stability -- these are attributes that set 
|vector_edition| apart in the crowded field of Linux distributions. There are
five editions to choose from. At the links below you will find information 
about the VL Edition best suited to your needs:

.. toctree::
   :maxdepth: 1
   
   vl-deluxe
   vl-soho
   vl-light
   vl-live
   vlocity

To download any version other than the Deluxe Edition please visit our
`download page <http://www.vectorlinux.com/downloads>`_. To obtain the
VL Deluxe Edition please visit our `CD
Store <http://vectorlinux.com/cd-store>`_.

Installation is quick and easy, and the end result is a system that
caters to 'newbies' and Linux-gurus alike. In addition, VL provides a
clean, powerful base for experts to tweak, and is an excellent distro
for users who are keen to learn what goes on 'behind the scenes' of
their operating system.


Documentation Manuals
=====================

.. toctree::
   :maxdepth: 1
   :numbered:
    
   manuals/installation_guide
   manuals/vasm
   manuals/slapt_get
   manuals/utilities
   manuals/mounting_guide
   manuals/printing_guide
   manuals/filesystem
   manuals/desktops
   manuals/lxc-containers
   manuals/docker
   manuals/sandboxing
   manuals/kernel-upgrade
   manuals/bootable_usb
   manuals/broadcom-wifi
   manuals/installation_debug_guide
   manuals/power-management
   manuals/contributing

Packaging
=========

.. toctree::
   :maxdepth: 1
   :numbered:

   manuals/packaging


Search documentation
====================

* :ref:`search`


Links
=====

-  `VectorLinux Homepage. <http://www.vectorlinux.com/>`_ Our online
   headquarters.
-  `VectorLinux Forum. <http://forum.vectorlinux.com>`_ Come and meet
   the VectorLinux community here. We are friendly and helpful and there
   is a wealth of useful information contained within. The developers
   frequent the forum every day. We believe it to be one of the best
   support communities on the planet!
-  `VectorLinux Knowledge and Support Centre
   WIKI. <http://vector.ecosq.com/ic/Home?menu=24>`_ Provides answers on
   how to do common tasks.
-  `VectorLinux HOWTO
   Forum. <http://forum.vectorlinux.com/index.php?board=10.0>`_ Provides
   HOWTO's for more difficult tasks.
-  `VectorLinux IRC Channel. <irc://irc.freenode.net/#vectorlinux>`_ At
   this Internet Relay Chat channel you will often find one of more of
   the VL developers who would be happy to answer your questions.
-  `Cheat Sheet. <http://vectorlinux.osuosl.org/Uelsk8s/test/>`_ Some
   useful commands for VectorLinux.
-  `Direct Paid Support. <https://vector.ecosq.com/ic/Support>`_
   Professional support services for VectorLinux.

