Create Bootable USB
===================


This is the official method of making a bootable usb stick for VectorLinux.

- Insert an USB stick and type as root

    ``fdisk -l``


In the fdisk output, check what drive designation your USB stick is, we'll use
``/dev/sdb`` as the example for this post.

- Change directory to where the iso is located

    ``cd /home/user``


Now this next step will hose your system if you accidentally use the wrong path
so make sure that you use the correct drive designation from fdisk -l and
remember I'm using ``/dev/sdb`` as an example, yours may or may not be
different!

- Run this command from a terminal (mat take a couple of minutes)

    ``dd if=name.of.iso of=/dev/sdb`` 


.. warning::

    Double check the path to your USB device, you can accidentally delete your
    data with this command.


And that's it, now you have a bootable USB stick which you can install from.

