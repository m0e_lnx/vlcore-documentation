========================================
|vector_edition| 7.x Broadcom wifi howto
========================================

Version 7.1 needs a little tweaking to get broadcom going. instructions are here
http://vlcore.vectorlinux.com:10080/m0e.lnx/sta-builder

The following applies to both 7.1 AND 7.2



Blacklist modules
=================
1. ``nano /etc/modprobe.d/blacklist.conf``

2. Add these lines to your ``blacklist.conf`` file

 ::

    blacklist b43
    blacklist ssb
    blacklist bcma
    blacklist bcm43xx
    blacklist bcm80211
    blacklist brcmsmac

3. Reboot. You should now have module wl loaded.
   Verify with ``lsmod | grep wl``

4. Done
