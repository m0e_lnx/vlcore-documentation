#####################################
Docker containers on |vector_edition|
#####################################

============
Introduction
============

About Docker
------------

According to the `official docker website <http://www.docker.com>`_,
docker is an open platform for application developers and system
administrators to build, ship, and run distributed applications.

Chances are, if you are reading this document, you know what it is, but 
just in case, see http://www.docker.com for more information.


About this guide
----------------

This document will guide you through the process of installing docker on
your |vector_edition| installation and configuring the system to run docker
containers.  Due to the fact that there is plenty of documentation on how to
use the docker platform all over the internet, I decided to save my 
fingertips and not cover every aspect of 'how to use docker' here.


A word or caution
-----------------

Docker is **not supported on |vector_edition| version 7.0  or earlier**.
It will most likely continue to be supported on future releases, but there are
no plans to backport this to earlier versions.


==================
Preparing the host
==================

Before any docker containers can be launched, we need to make some 
modifications to the host Operating System. 

Software
=========

The following packages are needed for docker support.

#. docker

Search for the package(s) and install them via GSlapt or issue the
following command on a terminal:

::

   slapt-get -u
   slapt-get -i docker

.. note::

   By default, docker is only available to the root user.  If you want to make
   it available to non-root users, add the user to the `docker` group via the
   usermod command.  ``usermod -a -G docker <your_user_login>``


Control Groups
==============


As of the time of this writing, the default cgroup configuration will
not work for supporting docker, so we need to make some changes to the
system.  This should be very easy if you follow this procedure and run
the following commands on a terminal as **root** (or copy and paste
from here)

Create new cgroup hierarchy (|vector_edition| 7.1)
--------------------------------------------------

For cgroups, docker does not support mounting cgroups on the "all" single
hierarchy controller, but rather the indivirual controllers.  That means
we need to create a new hierarchy to use for cgroup mounting because the 
default |vector_edition| does not have this setup by default in version 7.1.
 This can be accomplished by running this command on a terminal:

``mkdir -p /cgroup/{cpuset,cpu,cpuacct,memory,devices,freezer,net_cls,blkio}``

Modify your /etc/fstab (|vector_edition| 7.1)
---------------------------------------------

Open your /etc/fstab and add the following lines

::

   # For docker compatibility
   cgroup /cgroup/cpuset cgroup rw,relatime,cpuset 0 0
   cgroup /cgroup/cpu cgroup rw,relatime,cpu 0 0
   cgroup /cgroup/cpuacct cgroup rw,relatime,cpuacct 0 0
   cgroup /cgroup/memory cgroup rw,relatime,memory 0 0
   cgroup /cgroup/devices cgroup rw,relatime,devices 0 0
   cgroup /cgroup/freezer cgroup rw,relatime,freezer 0 0
   cgroup /cgroup/net_cls cgroup rw,relatime,net_cls 0 0
   cgroup /cgroup/blkio cgroup rw,relatime,blkio 0 0


Control groups config file (|vector_edition| 7.1)
-------------------------------------------------

Add the following content to your /etc/cgconfig.conf file.  If the file does
not exist yet, create it with this content.

::

   mount {
   cpuset = /cgroup/cpuset;
   cpu = /cgroup/cpu;
   cpuacct = /cgroup/cpuacct;
   memory = /cgroup/memory;
   devices = /cgroup/devices;
   freezer = /cgroup/freezer;
   net_cls = /cgroup/net_cls;
   blkio = /cgroup/blkio;
   }


System init script (|vector_edition| 7.1)
-----------------------------------------

One last thing to do is to modify the init system so that it respects the 
instructions set forth in /etc/fstab.  You will need to edit your 
/etc/rc.d/rc.S file and apply this patch (save as text and apply using 
``patch -Np0 < your_saved_text_file --verbose`` if you want)

::
	

    --- rc.S.dist   2014-07-30 13:21:16.000000000 +0000
    +++ rc.S        2014-07-30 13:21:48.000000000 +0000
    @@ -61,15 +61,15 @@
       /bin/plymouth --show-splash
     fi
     
    -# Mount Control Groups filesystem interface:
    -if grep -wq cgroup /proc/filesystems ; then
    -  if [ -d /sys/fs/cgroup ]; then
    -    mount -t cgroup cgroup /sys/fs/cgroup
    -  else
    -    mkdir -p /dev/cgroup
    -    mount -t cgroup cgroup /dev/cgroup
    -  fi
    -fi
    +## Mount Control Groups filesystem interface:
    +#if grep -wq cgroup /proc/filesystems ; then
    +#  if [ -d /sys/fs/cgroup ]; then
    +#    mount -t cgroup cgroup /sys/fs/cgroup
    +#  else
    +#    mkdir -p /dev/cgroup
    +#    mount -t cgroup cgroup /dev/cgroup
    +#  fi
    +#fi
     
     # Start file systems
     echoc "Mounting local file systems" cyan


Finalizing host configuration
=============================

After finishing all the previous steps, you will need to restart your computer
to get the right cgroups configuration.  Continue to the ``Testing your setup``
section after the system restarts.


==================
Testing your setup
==================

At this point, your host operating system should be ready to launch docker
containers.  Follow these steps to verify your system is ready.

Start the docker service daemon
-------------------------------

::

   /etc/rc.d/rc.docker start
   ps ax |grep docker.pid

That should print a line indicating the docker daemon is running and you should
also have a ``/var/run/docker.pid`` file with a numeric value in it.


Networking
----------

After the docker daemon starts, it will create its own network bridge
(typically labeled ``docker0``).  You can verify this by issuing the 
``ifconfig`` command on a terminal.  The output should display a 
``docker0`` entry.

If both of these tests passed, then you are ready to use docker on your
|vector_edition| installation.


============
Using Docker
============

Since there is plenty of documentation out there on how to use docker, I will
not waste my breath trying to cover that aspect in depth.  This will just
be a quick guide on how to download an image, create a container from it and
launch a shell inside it.

Download an OS image
--------------------

The docker image repository has lots and lots of images to choose from.  For 
this example, we will be using a slackware64-14.1 image hosted at the public 
image repository.  To see the complete listing issue ``docker search <term>``
to search for any distro image that interests you.

::
   
   docker pull vbatts/slackware

That will download the slackware image and copy it to your local filesystem
for docker to use.

Run a docker container
----------------------

After the ``vbatts/slackware`` container has been downloaded, you are ready
to launch a container using it.  To start it issue the following command.

::

   docker run -t -i vbatts/slackware /bin/bash

That will give you a new shell prompt with a very bare slackware setup.


Official |vector_edition| image
-------------------------------

As of this writing, |vector_edition| does not yet have an official image on
the docker public image repository.  A test image has been prepared and
uploaded to our public package repository for testing purposes.  Keep in mind
this is barely getting implemented, so it is possible that the image will
change if required later.  It is also possible that these images will be 
available from the docker public image repository later too.  To import
the testing |vector_edition| image, open a terminal and issue the following
commands

First `download an image
<http://vlcore.vectorlinux.com/pkg/vlbuildslave/docker-images>`_.

Then, import the downloaded image into your docker images collection.
For this example we will assume you downloaded the 32-bit docker image
for VectorLinux 7.1.  File name is ``VL-7.1-BB-FINAL_vlbb-docker.tar.xz``

::
   
   cat VL-7.1-BB-FINAL_vlbb-docker.tar.xz | docker import - vector:7.1-bb

After downloading and importing the image, issue ``docker images`` and you
should see a line displaying the newly imported image at the top of the listing.

+-----------+--------+----------+---------+-------------+
|REPOSITORY | TAG    | IMAGE ID | CREATED | VIRTUAL SIZE|
+===========+========+==========+=========+=============+
| vector    | 7.1-bb | 154ad... | about.. | 69.36 MB    |
+-----------+--------+----------+---------+-------------+


After the image is imported, you may run it as usual.

::

   docker run -ti vector:7.1-bb /bin/bash

.. note::
   See ``docker help run`` for more information on all arguments available to
   the ``docker run`` command.  You can also issue ``docker help`` for
   more help on all other docker operations.

