=====================================
|vector_edition| Kernel Upgrade Guide
=====================================

Vector Linux offers several kernels. Here i will explain two methods
of how to add a kernel to your system. I say **ADD** because you should 
**NOT** get rid of your working kernel. If for some reason something
goes wrong you will have the original kernel to fall back on.


Method 1
========

For this guide, 7.1 is used as an example. the kernel names for 7.2 have changed 
to ``kernel4.x``, `` kernel4.x-modules``, etc.


First of all you need to download a kernel package:
- 32 bit kernels:
  http://vlcore.vectorlinux.com/pkg/stable/veclinux-7.1/extra/kernels/
- 64 bit kernels:
  http://vlcore.vectorlinux.com/pkg/stable/VL64-7.1/extra/kernels/

Then follow these instructions:

- Download the kernel package and the **matching** kernel-modules 
  package

- ``cd`` to the directory you saved the kernel to

- ``installpkg`` both the kernel and kernel-modules packages



Method 2
========

- Add the apropriate repo to your /etc/slapt-get/slapt-getrc
- ``slapt-get -u``
- ``slapt-get -search kernel``

decide which kernel and its matching modules you want to install

- ``slapt-get -i --no-upgrade kernel-version``
- ``slapt-get -i --no-upgrade kernel-version-modules``


Update your bootloader Menu
===========================

Again, kernel 4.0.7 is only used as an example. this will work with any kernel version


If you are using LiLO:
----------------------

- If you use lilo open ``/etc/lilo.conf`` with your editor of choice
- Simply copy the first kernel entry section which looks like this::

    # -- Vector on /dev/sdX --
    image = /boot/vmlinuz
    label = Vector
    initrd = /boot/initrd.gz
    root = "UUID=3b3ed16e-8db3-44be-b4b8-9b81df7692b2"
    append = "quiet splash"
    read-only

    # --

- Paste it at the bottom of ``lilo.conf`` and edit as appropriate
- For example if you installed kernel 4.0.7::

    # -- Vector on /dev/sdX -- 
    image = /boot/vmlinuz-4.0.7
    label = Vector-4.0.7
    initrd = /boot/initrd.gz
    root = "UUID=3b3ed16e-8db3-44be-b4b8-9b81df7692b2"
    append = "quiet splash"
    read-only
    # --

Now run ``lilo``

- ``lilo -v``


or if you use grub

- ``grub-mkconfig -o /boot/grub/grub.cfg``


That's it. Reboot and you should now have an option to boot the new 
kernel as well as the original kernel


If you are using GRUB2
----------------------

Update the Grub2 menu by running ``grub-mkconfig -o /boot/grub/grub.cfg`` as
root in a terminal.

That's it. Reboot and you should now have an option to boot 4.0.7 as well as the original kernel
