##################################
LXC Containers on |vector_edition|
##################################

lxc-containers has been deprecated in favor of docker.

See docker documentation

.. toctree::
   docker

