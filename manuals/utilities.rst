
###########################################
|vector_edition| System Utilities and Tools
###########################################

|vector_edition| v6.0 provides users with a number of system tools and
utilities to make administration of their computer system easier than
hand-editing configuration files. These utilities are described in
detail below, but for the most part, their usage is trivial and obvious.
Where that is not the case the intricacies are explained.


VasmCC
======

VasmCC is the |vector_edition| (System) Control Centre. It provides an iconic
GUI interface to vasm. VASM is the Vector Administration and System
Menu, fully explained in the `VASM manual <vl6_admin_vasm_en.html>`_.
"vasm" itself is an ncurses based text menu interface, while VasmCC is
simply a GUI wrapper to vasm. The image below show the basic
functionality of this software.

.. image:: ../_static/images/utilities/vasmcc.jpg
   :align: center
   :alt: VasmCC


Vpackager
=========

vpackager is a |vector_edition| utility which enables users to build a
properly packaged VL .tlz package from source code. A complete
explanation of the VL package management system (gslapt and slapt-get)
is found in our `slapt-get/gslapt/packages
manual <vl6_slaptget_en.html>`_.

The user is also referred to our forum where a full explanation on `how
to build a
package <http://forum.vectorlinux.com/index.php?topic=1380.0>`_ is
described.
vpackager documentation is fully explained at the `vpackager
website <http://code.google.com/p/vpackager/wiki/HowToUse>`_, so will
not be fully re-iterated here. Newer Linux users should be aware that
vpackager is not required to install or remove packages. Such
functionality is provided instead by "gslapt". The image below shows the
basic functionality of vpackager.

.. image:: ../_static/images/utilities/vpackager1.gif
   :alt: Vpackager
   :align: center


Vcpufreq
========

"vcpufreq" is a Central Processing Unit (CPU) frequency configuration
utility developed in Gambas by Joe1962 for |vector_edition|. It makes it easy
to change the CPU frequency governor (with those CPU's which have such a
function) and related frequency settings. It does not (yet) load the
appropriate cpufreq module for your system. vcpufreq includes the CLI
utility "vcpufreq-load" to set the saved configuration on system start.
This utility requires root access and requires that the appropriate
module is loaded. With most modern CPU's this will be done at boot up.
If this is not the case then the user must find out which module is
relevant and works with their CPU and then use the command "modprobe
name\_of\_module" in their /etc/rc.d/rc.local file, to ensure the module
is loaded at boot time. A listing of the available CPU modules are found
in the directory /usr/src/linux-2.6.???/arch/i386/kernel/cpu/cpufreq.
vcpufreq would be useful to those with notebook computers in order to
reduce CPU frequency (and hence battery usage). The image below shows
the basic functionality of vcpufreq.

.. image:: ../_static/images/utilities/vcpufreq1.gif
   :alt: vcpufreq
   :align: center


Firestarter
===========

"Firestarter" makes configuring a firewall for Linux easy and replaces
the older VL utility "vleasytables" for VL version 6.0. Nonetheless,
newbies should know exactly what a `firewall
is <http://en.wikipedia.org/wiki/Firewall>`_ before proceeding to enable
a firewall under |vector_edition|. If you are behind a hardware router, you
are not serving files to other machines, you the only user of your
machine, are a generally careful user of the Internet and do not
normally run your computer as root user then a firewall may not be
necessary. Otherwise enable a firewall. For more details please read the
`Firestarter documentation <http://www.fs-security.com/docs.php>`_.

The image below shows the status interface screen of Firestarter.

.. image:: ../_static/images/utilities/firestarter.jpg
   :alt: Firestarter
   :align: center


HAL, Vl-Hot and Vl-Hot-config
=============================

HAL and vl-hot are utilities which enable auto-detection and
auto-mounting of devices such as CDROM drives, external hard drives, USB
devices, etc. HAL (Hardware Abstraction Layer) is a daemon which
provides an easy way for applications to discover the hardware on the
system. It basically detects new hardware or inserted drives and media
and automatically pops up the software necessary to handle the new
device.
`vl-hot <http://broadcast.oreilly.com/2009/02/vl-hot-a-non-polling-alternati.html>`_
is a purely udev based automount system for any kind of pluggable
storage device that conforms to the block device specification and so
uses scsi emulation. Both utilities do much the same thing but vl-hot is
much less resource intensive. HAL is the default application installed -
if you have a slower computer or enjoy less resource intensive
applications then you can go into "vasm" and change the HAL system to
vl-hot.

"vl-hot-config" provides simple configuration to the |vector_edition|
automounting system called vl-hot. Hardware known to work with vl-hot
are USB pendrives, hard disks, digital cameras, memory card readers,
PCCARD (or PCMCIA) memory cards and external USB drives. Firewire
devices should work, but there are no user reports on this kind of
hardware yet. More information on vl-hot and its differences to the HAL
automount system are found in the `VL mounting
manual <vl6_mounting_guide_en.html>`_.

The working specifications of vl-hot are such that the mount base
directory will be "/mnt/vl-hot/", each drive will have a "sd?" directory
(where "?" represents a letter of the alphabet) and each mounted
partition will have a "vol#" directory (where "#" represents a number)
within the drive directory. An exception to this rule is in the case of
non-MBR devices, where there is no partition table. Desktop icons are
dynamically created/deleted (currently only for KDE, xfce and LXDE) and
these have an unmount options in the context (right-click) menu. (In VL
Light 6.0 using IceWM of JWM window managers you can get to the device
by opening the "My Documents" icon and check the right hand panel for
your device). There is a conventional unmount (should be used normally)
as well as options for unmount with signalling to the process that is
holding open the mount and unmount with killing of said process (the
last two should be tried in that order, when the normal unmount fails).
The unmount operation has completed successfully once the desktop icon
disappears.

vl-hot-config enables the user to change the base mount directory, the
mount icon, what should be included and automatically excluded from the
mount process, the log file locations, sound notifications and mount
specifics depending on filesystem type.

The image below shows the basic functionality of vl-hot-config.

.. image:: ../_static/images/utilities/vl-hot-config1.gif
   :alt: vl-hot-config
   :align: center


Vlsmbmount
==========

"vlsmbmount" is simply a front end menu message to the xsmb.sh shell
script, which assists a user in connecting their computers via Samba.
For this system to work the user must have the smbd and nmbd service
daemons running beforehand. To do so go to System, VASM, SUPER, SERVICE,
SRVSET, 4 and enable the Samba daemon.

The images below shows the basic functionality of vlsmbmount.

.. image:: ../_static/images/utilities/vsmbmount1.gif
   :alt: vlsmbmount
   :align: center


Vwifi
=====

"vwifi" is a relatively simple bash script which attempts to detect the
user's wireless card device and to launch the wifi services via the
iwconfig, dhcpcd and ifconfig network commands. If it does not find an
appropriate module for your device it will ask you for an appropriate
one from a supplied list. The user must know what module to choose. To
determine the correct module you must know what chipset your wireless
device uses. This can often be determine with the "lspci" command. Then
visit `this Linux wireless site <http://www.linux-wlan.org>`_ for more
information on your chipset and the appropriate module to use. Any
further questions regarding problems about VL and wireless might be
solved by reading `our wireless
HOWTO <http://www.vectorlinux.com/forum2/index.php?topic=1282.0>`_.


Wicd
====

"Wicd" is enabled by default on |vector_edition| 6.0. It enables one to scan
for available networks and create profiles for your preferred networks.
At boot time, running Wicd as a continuous daemon will automatically
scan for an available preferred network and connect to it. For further
information visit the `Wicd website <http://wicd.sourceforge.net>`_ or
`here <https://help.ubuntu.com/community/WICD>`_.

The image below shows the basic functionality of Wicd.

.. image:: ../_static/images/utilities/wicd.jpg
   :alt: wicd
   :align: center


Slapt-get-notifier
==================

"slapt-get-notifier" is a small daemon enabled by default on |vector_edition|
6.0. It notifies the user about package updates available via slapt-get
and gslapt. It is modeled after the Ubuntu update-notifier.
slapt-update-notifier places an icon in the user's notification area
when updates are available. Clicking the icon starts upgrading with
gslapt.


Vburn & Tburn
=============

"vburn" is a small, simple GUI optical drive burning application.
"tburn" is its console based equivalent. They are for CD burners and not
for DVD burning. For that functionality please use "k3b". vburn & tburn
are examples of very slim, trim and fast Linux applications.

The image below shows the basic functionality of vburn.

.. image:: ../_static/images/utilities/vburn.jpg
   :alt: vburn
   :align: center


GDM
===

"gdm" is the login manager application now used on VL 6.0 Standard.
Previous users will note this change because either the kdm and xdm
login managers were used on previous editions of VL Standard. The main
differences to note with gdm are that one may actually change languages
within gdm, configure gdm itself from the login screen and enable a
remote login. gdm is highly customizable but this requires reading its
`documentation <http://library.gnome.org/admin/gdm/2.24/>`_.

.. versionadded:: 6.0
   Since version 6.0 GDM is used instead KDM and XDM

The image below shows the basic functionality of gdm.

.. image:: ../_static/images/gdm.jpg
   :alt: GDM
   :align: center

