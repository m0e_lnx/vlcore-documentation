
##########################################
System Administration with VASM and VasmCC
##########################################


Introduction
============

"VASM" is short for "Vector Administrative and Services Menu" and is
Vector Linux's main system configuration tool. Recently, VL has added
VASMCC to version 6.0. VASMCC provides a very newbie-friendly GUI iconic
interface to the VASM menu system, making it easier for the new user to
find his/her way around VL system administration commands. It is
recommended that newbies and users new to VL use VasmCC for their system
administration work.

VASM is the same in both CLI (text) and GUI mode. The script
automatically identifies the mode you are currently in and uses the
appropriate interface.

|image1|

*VASM running in Graphical Mode.*

.. note::
    1.For most functions VASM needs you to be logged in as root, the
    administration account on your VectorLinux system.
    2.Some functions accessible through the VASM require the system to be
    running in Text Mode and not GUI Mode. Such functions are clearly marked
    and cannot function in GUI mode. (See Section 5 below).

|image2|
 
*VASM running in Text Mode.*


User features vs. Root features
===============================

You can use VASM as a normal user or as privileged root user (System
Administrator). The difference is that running it as a normal user gives
you access to only those functions that affect that particular user,
whereas in root mode you can access the full range of options that
affect the entire system and/or all of the users.

If you launch VASM as a normal user, you may access the root mode by
selecting the "SUPER" entry in the menu. You will then be required to
enter the root´s password to be granted permission. This is, of course,
a security measure.

.. note:: 
    The tools available to normal users are a subset of those
    available to root user. This document explains the complete
    functionality of VASM.


Use in X-Windows (GUI)
======================

If you have started your window manager of choice, double-click on the
VASM icon or from the main menu select "System", then select "Vector
Administrative and Services Menu or VASM" and VASM will then be loaded
onto your screen.

Depending on your VL version, you may have an icon on your desktop
labeled "*Vector Administrative and Services Menu*", click it and you
will presented with VASM´s interface.

If you are running the system as a regular user you will see an entry
named "SUPER" to access to the root level, select it and you will be
prompted to enter the root´s password.

Please, be aware that some functions in VASM require the system to be
running in Text Mode and not GUI Mode (see chapter 5).


VasmCC (GUI wrapper)
====================

The volunteer developers at VectorLinux have created a very user
friendly GUI system interface to VASM called VasmCC or the "Vector
Administration and System Menu Control Centre". Most users will now
probably prefer to use this system front end to VASM. It provides a well
structured iconic interface which makes finding the right system
administration tool easy.

The screenshots below show the various screens of VasmCC and the
utilities available within each screen. Clicking the various utilities
raises the appropriate VASM menu interface - all of which are described
in detail later in this document:

|image3|

*Utilities*

|image4|

*Filesystem*

|image5|

*XWindow*

|image6|

*Network*

|image7|

*Hardware*

|image8|

*Service*

|image9|

*User*


Use in Text Mode
================

To launch it, type in console mode "vasm" (lowercase) without quotes and
hit enter. This will start an ncurses based menu. Use the cursor [up]
[down] keys to navigate the menu. Press the [enter] key to make a
selection. If you wish to exit use the [left] [right] arrows to select
CANCEL.

If you are running the system as a regular user you will see an entry
named "SUPER" to access to the root level. Select it and you will be
prompted to enter the root´s password.


Switching to Text Mode
======================

For some of the administrative tasks available in VASM, like configuring
the X Server, hardware autodetection, etc., it is safer, or even
strictly required, that the system is running in text mode (Run Level
2). When this situation appears, VASM will let you know and you will not
be able to perform such tasks from your window manager.

If you are running in GUI mode, log out of it, then:

-  If you are taken to the text mode console, you can call vasm from
   there.
   (This is the case when your system is configured to boot
   automatically into Text Mode (also known as TUI Mode).
-  If you are presented with a GUI login, this means that the system is
   configured to autostart in GUI mode (runlevel 4 or 5).
   In this case you need to switch into TUI mode (runlevel 2/3).
   Please press Ctrl + Alt + F1, login as root, and type "init 2", the
   system will shutdown the X server.
   You may then launch vasm and configure the system.
   Once you have finished, switch back to GUI mode again by typing
   "init 4".


Detailed descriptions
=====================

This section explains, in order, each of the configuration utilities
available to the root user in VASM.

AUTOSETUP
---------

Autosetup goes through the same autodetection and autoconfiguration
process that was performed at installation time. Doing this means
resetting your system to almost fresh install status. Therefore some of
the configurations post-install that you may have done to the system
could be re-set to some default value.

If you need to reconfigure something on your system it is advisable to
use the specific VASM entry for that task.


USER
----

With this section of VASM you can manage users. You have to be logged in
as administrator of the system, so login as 'root' via console or SUPER
option in VASM.

**Command line:** /sbin/vuser

|image10|

*User administration menu in Graphical Mode.*

ADD
~~~

**Purpose:** Add a new user to the system.

**Command line:** /sbin/vuseradd

First you will be asked to enter a login for this new user.
Just enter the name you want (lower case) and press the 'enter' key or
click on the 'Ok' button.

The next dialog will ask you to enter the real name of the person
corresponding with the login name you entered in the previous dialog.
Enter the name and press 'enter' or click on the 'Next' button.
If you want to change the login name at this stage you can click on the
'Previous' button.

The following dialog will ask you to enter an id for this user. Choose a
unique id for the user you want to add or just accept the default by
pressing enter for the next dialog.

You will get a list of groups the user can be a member of. Select the
groups the new user must have access to. Mostly you can accept the
defaults. Deselect the groups to limit the access rights.

In a fresh vl6.x install the following groups are available:
- cdrom: user can access CD/CDW/DVD
- floppy: user can access floppy disks
- lp: user can access printers
- audio: user can access audio devices
- video: user can access video devices
- games: user can play restricted games
- adm: administrators
- sys: system administrators
- wheel: elite users

Click on 'Ok' if you're finished setting the groups.

Now you will get a view of the login name, real name, user id etc.
You will be asked if you want to create this user on your system. Press
'enter' to accept the input you gave.

The last step is to enter a password for the new user.
Enter a password for the new user (it is more secure to choose a
combination of at least 6 characters, numbers and special characters,
e.g. c@vi@47 ). Press 'enter' or click on 'Ok' to accept the password.

In the dialog that follows, you will be asked to re-enter the password
you gave. Press 'enter' or click on 'Ok' after you entered the password
again.

The last dialog confirms the creation of this new user on the system.
After clicking on 'Ok' you will return to the USER ADMINISTRATION menu.


DEL
~~~

**Purpose:** delete a user from the system.

**Command line:** /sbin/vuserdel

After you choose this option you will get a list of the users on the
system.
Select the user you want to delete (double click on the user or select
with the arrows up/down and press 'enter').
A dialog pops which displays the user information and asks if you want
to delete that user. Press 'enter' or click on 'Yes' to confirm.
You will return to the list of users that can be deleted. Click on
'Cancel' to return to the USER ADMINISTRATION menu.


PASSWD
~~~~~~

**Purpose:** change the password of an user.

**Command line:** /sbin/vpasswd

With this option you can change the password of a user. Select the user
from the list (double click on the user or select with the arrows
up/down and press 'enter'). Enter the new password for this user and
press 'enter' or click on the 'Ok' button. In the dialog that follows,
re-enter the password you gave in the previous dialog and press 'enter'
or click on 'Ok'. If the password matches you will see a confirmation of
the change.

You will return to the USER ADMINISTRATION menu.


PASSROOT
~~~~~~~~

**Purpose:** Change the password of 'root', the administrator account of
your Vector Linux system.

Enter a new password for 'root' in the first dialog and re-enter the
password in the second dialog. See 6.2.3 PASSWD.


XWINDOW
~~~~~~~

This section deals with the configuration of the X window system. You
will need to be running the system in console mode (see Section 5).

|image11|

*GUI administration menu in Graphical Mode.*


XCONF
~~~~~

**Purpose:** Configure the system´s X server for compatibility with your
video card, monitor and mouse hardware.

**Command line:** /sbin/vxconf

**Walk Through:** When you enter the Xconf section you can choose
between three options:

-  **Auto**
   This will detect and configure your video card and monitor, ask a
   few questions about your desired resolution and mouse
   characteristics, and will create a NEW configuration file. Any
   previous manual adjustments you may had done to that file will be
   overwritten.
-  **Current**
   This will perform the detection and ask you some details about your
   preferred resolution, colour depth, and mouse configuration. However,
   it will commit those to the configuration file (*/etc/X11/xorg.conf*)
   without deleting the file and will thus keep **some** additional
   configuration lines you might have edited.
   For instance, as it does not ask for your keyboard layout, there
   won't be any changes to your keyboard configuration, but since it
   asks you to choose between the detected available screen resolutions,
   those will be written to the file, overlapping any previously
   configured value.
-  **Basic**
   This option will mostly behave in the same way but configures the
   system to use the VESA driver. This driver is as a 'safe mode' video
   driver (although a very good one, capable of over 800x600 and 16
   bits) and is most useful for cards that do not have native linux
   drivers or which refuse to be properly detected. This option should
   at least allow you to get a basic graphical environment up and
   running.


XDMSET
~~~~~~

**Purpose:** allows you to choose the **X Display Manager**, that is,
the utility in which you log in when the system boots automatically to
GUI mode.

**Command line:** /sbin/vxdmset

**Walk Through:** Select the one you prefer, and press "Ok". You will be
asked if you want to test it at that moment, something that requires the
X server to be restarted, so you will have to close any running
applications.

If you choose not to test it, you will be taken back to the previous
menu.


XWMSET
~~~~~~

**Purpose:** Sets up which **Window Manager** is launched from the
console when you type "startx" and your system is configured to boot in
text mode.

**Command Line:** /sbin/vxwmset

**Walk Through:** When you choose this option, you will be given a list
of available window managers to start when you use startx. Make a
selection or cancel.


BOOTSET
~~~~~~~

**Purpose:** configure you system to boot directly into GUI Mode or Text
mode (these are technically known as "runlevels").

**Command Line:** /sbin/vbootset

**Walk Through:** When you select this option, you will be given the
choice between "text mode desktop", "text mode server", "GUI mode
desktop" or "GUI mode server". The difference between desktop and server
choices is in the services they start, i.e. ssh, apache, and the like.
Independently of this menu, you can further configure which services are
started or not selecting "Service" then "Srvset" in VASM´s main menu.

**Warning:** Ensure X-windows works before setting the system to start
in graphics mode (try "startx" before defaulting to graphics mode).


SERVICE
-------

Allows you to configure services started at boot time.

**Command Line:** /sbin/vsrvmenu

|image12|

*SERVICES menu in Graphical Mode.*


BOOTSET
~~~~~~~

**Purpose:** configure your system to boot directly into GUI Mode or
Text mode (these are technically known as "runlevels")

**Command Line:** /sbin/vbootset

.. note:: 
    This option is also present in the XWINDOW Menu.

**Walk Through:** When you select this option, you will be given the
choice between "text mode desktop", "text mode server", "GUI mode
desktop" or "GUI mode server". The difference between desktop and server
choices is in the services they start, i.e. ssh, apache, and the like.
Independently of this menu, you can further configure **which** services
are started or not selecting "Service" then "Srvset" in VASM´s main
menu.

**Warning:** Ensure X-windows works before setting the system to start
in graphics mode (try "startx" before defaulting to graphics mode).


INITSET
~~~~~~~

**Purpose:** Allows you to choose the services **initialization method**
to use when you switch runlevels with the "init <run\_level>" command.

**Command line:** /sbin/vinitset

**Walk Through:** the initialization system starts or stops services
when you switch run levels using the "init <run\_level>" command. The
standard initialization system is called SysV-Init.

When switching run levels, it kills all services of the previous level
then starts all services of the new level. Vector Linux offers a
**faster** init system: It does not kill the services only to start them
again in the next runlevel. So here you can choose what method you
prefer.


HWSET
~~~~~

**Purpose:** This menu allows you to enable or disable the
initialization of hardware components of your system.

**Command line:** /sbin/vhwset

**Walk Through:** When selecting this entry a list will appear, showing
the hardware you can initialize at boot time. This means that if you
plan to use a parallel port printer you need to set "parallel" to be
initialized at boot time, but if you don´t plan to, you can disable it
and free some system resources. The available options are:

-  **PNP**
    ISA Plug and Play. This should be enabled for a modern PC
-  **Hotplug**
    USB/PCI hotplug devices (pendrive, digital camera, etc)
-  **PCMCIA**
    PCMCIA cards, common on laptops (network, modem, etc.)
-  **APM**
    Advanced Power Management, useful for Laptops.
-  **Serial**
    Serial port and modem
-  **Parallel**
    Parallel port and printer
-  **Wireless**
    Networking using radio or infrared
-  **ALSA**
    Advanced Linux Sound System
-  **udev**
    Dynamic /dev naming for hotplug on kernel 2.6
-  **tmpfs**
    On memory /tmp. Recommended if RAM>=196MB + SWAP>=256MB

**Screenshot:**

|image13|

*Selecting system components to be initialised at boot time.*


SRVSET
~~~~~~

**Purpose:** Here you can configure what services, such as ssh server,
http server, etc., are started in different runlevels.

**Command line:** /sbin/vsrvset

**Walk Through:** You are presented with a list of the runlevels. Select
the one you wish to configure and press Enter. Then you will see the
available services and which ones are set to be started when the system
enters that specific runlevel. Toggle the ones you want to enable or
disable and press "Ok".

You can modify services for other runlevels or exit the menu.

**Screenshot:**

|image14|

*Selecting services to start at runlevel 5.*


CUPSWEB
~~~~~~~

**Purpose:** Allows you to add and configure local and networked
printers. It requires the CUPS package to have been installed and the
CUPS daemon to have been started on your system. If you need to install
it, please refer to the `VectorLinux Printing
Guide <vl6_printing_guide_en.html>`_, where you will find more detailed
instructions.

**Command line:** /sbin/vcupsweb

**Walk Through:** This option launches the web browser and attempts to
connect to the local port for CUPS. There, you will have the choice to
add, remove, and configure local and networked printers, print jobs,
etc. If you use this option from console mode, the launched browser will
be lynx. While lynx is very capable for the task, you might find it more
comfortable to use this option in GUI mode, where Firefox will be used.


SAMBAWEB
~~~~~~~~

**Purpose:** Allows you to configure file and printer sharing through
the network with other systems. Specifically for sharing between Windows
and Linux machines.

**Command line:** /sbin/vsambaweb

**Walk Through:** This option launches a web browser and connects to
"SWAT", an application that provides a relatively easy way to configure
file and printer sharing. If SWAT is not running you will asked whether
you want to start it or not. You need to accept in order to access its
web-based configuration interface.

You *can* use this in text mode, but the text browser will probably make
the task harder than it needs to be, so you are encouraged to do this in
GUI mode, where a more capable browser will be started.

.. note:: Dillo, a very light graphical web browser doesn´t support some
    required features, so you can´t access SWAT with it.


NETWORK
-------

Setup network configuration.

**Command Line:** /sbin/vnetmenu

|image15|

*NETWORK menu in Graphical Mode.*


NETCONF
~~~~~~~

**Purpose:** Walks you through setting up a basic network connection.
This program is only needed if you are connected to a LAN or a cable
modem network.

**Command Line:** /sbin/vnetconf

**Walk Through:**

When you select this option the program will prompt for information
required to set up the network connection.

1. Input computer name and domain name in the form of
   "computername.domainname". If you are not connected to a domain, that
   is, you are a lone computer workstation connected only to the Internet
   it is best to type in a computername only. The format "aaaa.bbbb.ccc" 
   is also valid, but note that you can shadow a public Iternet address by 
   using it.

2. IP address. You have three choices: Static, DHCP and Loopback. If you
   have a DHCP server (perhaps a router) select DHCP from the list (this is
   the case for most users connected to the Internet via an ADSL or cable
   modem). If you have no lan card choose loopback. If you wish to set a
   static address choose Static. If you choose Static you will then be
   prompted for the IP address for the machine and then the netmask of the
   network. Next you will be prompted for the IP address of the gateway to
   the Internet. Finally, you will be asked for the IP address of the name
   server.


NAME
~~~~

**Purpose:** Set hostname and DNS server

**Command Line:** /sbin/vnameset

**Walk Through:**

When you select this option the program will prompt for information
required to set up the network connection.

1. Input computer name and domain name in the form of
   "computername.domainname". If you are on a personal workstation not
   connected to other computers you do not require a domainname.

2. Input IP address of the the domain name server.


INET
~~~~

**Purpose:** Setup one or more network interfaces that will be
automatically started at system initialization. Each inet script may
have a different network device and TCP/IP setting.

**Command Line:** /sbin/vinet

**Walk Through:** When you select this option you will be provided a
menu of options.

-  **Add**
   This option is used to set up a network interface.
   1. When started the user will be given a list of configuration files
   that can be used for your connection. Once this has been chosen you
   are prompted for the settings for the connection.
   2. Now choose the network interface that you want to configure.
   Generally, you should chose a primary device (like eth0) from the
   list below. Use the alternate device (like eth1) for multiple IP
   configurations, mostly useful for a server or a laptop. Do not use
   the same device for more then one connection.
   3. Next you will be asked how the device get its IP address. Your
   choices are DHCP, Static or Probe.
   DHCP is the automatic method. Use it if you are connected to a
   dynamic network like cable modem, DSL or your administrator tells you
   to do so. Most home computers attached to the Internet with a small
   home network using a router and/or an ADSL, ISDN or Cable modem will
   use DHCP.
   Select STATIC only if you definetaly have a permanent IP address for
   this machine.
   PROBE is a STATIC method with network testing, useful for a laptop.
   If you choose DHCP, the setup is complete. If you choose one of the
   others you must assign the address. If you are connected to an
   official network (campus, office, etc), you must get the IP address
   from the system administrator. However, if you are on your own
   network, you may assign an arbitrary IP address from the standard
   internal IP range:
   192.168.0.1 - 192.168.255.254
   172.16.0.1 - 172.31.255.254
   10.0.0.1 10.255.255.254
   Next you will be asked for the network mask. They are of the form
   255.255.255.0. This will be provided by your system administrator. If
   you are on your own network the above should be sufficient.
   Finally you will need to supply the IP address of the gateway. A
   gateway is the computer that forwards network traffic to the bigger
   network (most likely the Internet). You need the IP address of the
   gateway. If you choose Probe the system will use this to verify to
   which network you are attached. Leave it empty if this inet does not
   have a gateway.
-  **Delete**
   Use this option to delete an unused inet configuration.
-  **Set**
   This option is used to set up a network interface. When started the
   user will be given a list of configuration files that can be changed.
   You are prompted for the settings for the connection. Follow steps 2
   to 6 in the add option.
-  **Start**
   This option is used to start manually start a connection.
-  **Stop**
   This option is used to start manually stop a connection.


WIRELESS
~~~~~~~~

**Purpose:** Manage wireless configuration files - change interface,
add, delete and change connections. These configuration files are stored
in /etc/wireless/.

The second group of options start, stop and give the status of a
connection. This is done via /etc/rc.d/rc.modules.

Use Network:INET option to set up the ethernet interface.

**Command Line:** /sbin/vinetset

**Walk Through:** When you select this option you will be provided a
menu of options.

-  **Interface**
   This option is used to change interface settings. You can identify
   which network interface is wireless. If you use ndiswrapper the
   interface is wlan0. If you use wpa\_supplicant you will need to
   identify which driver to use. (known to work for ndiswrapper with
   Broadcom driver (bcmwl5).
-  **Add**
   Use this option to add and define a new connection.
   When you select this option you will be asked for the ESSID of the
   connection. Connections are identified by their ESSIDs (Extended
   Service Set Identification). All of the wireless devices on a WLAN
   must employ the same ESSID in order to communicate with each other.
   This will be the identifier for the associated configuration file
   (*/etc/wireless/<ESSID Name>.essid.conf*).
   Next you will be asked for a short description for the connection.
   Next you be asked to choose the type of encryption. They include
   plaintext (no encryption), WEP and WPA (personal). If you choose WEP
   or WPA you will be asked for the encryption key to use. If you choose
   WPA supplicant a second configuration file is created
   (*/etc/wireless/<ESSID Name>.wpa.conf*). You can modify this file if
   you have specific requirements. Detailed documentation is available
   at
   `http://hostap.epitest.fi/wpa\_supplicant/ <http://hostap.epitest.fi/wpa_supplicant/>`_
   .
   Next you will be given a list of parameters that you may wish to set
   (including the description and encryption) specific to your own
   requirements.
-  **Del**
   Choose the connection definition that you wish to delete. Select the
   connection that you wish to delete. A backup copy is created
   (*/etc/wireless/<ESSID Name>.essid.bkp and /etc/wireless/<ESSID
   Name>.wpa.bkp*).
-  **Change**
   Use this option to change the parameter for the connection. Select
   the connection that you wish to change. A backup copy is created
   (*/etc/wireless/<ESSID Name>.essid.bkp and /etc/wireless/<ESSID
   Name>.wpa.bkp*).
-  **Connect**
   Select this option to connect an available ESSID. You will be given
   a list of visible ESSIDs. If none are visible the list will include
   the defined connections in case they do not broadcast their ESSID.
-  **Stop**
   Use this option to stop the wireless connection.
-  **Status**
   Select this option to get a detailed status of the wireless
   connection.

.. note::
    Assumes that the wireless interface has been installed and working.
    Configuration data is stored in */etc/wireless*. This directory is only
    available to root since it may contain passwords.
    In order to automate startup use HARDWARE:HWSET or by making
    */etc/rc.d/rc.wireless* executable.


MODEM
~~~~~

**Purpose:** Utility for configuring a dial-up connection to an ISP. You
need to know a number of things about your ISP (phone number,
authentication method, etc.) and your modem (device, baud rate, init
string, etc.)

**Command Line:** /sbin/vmodemset

**Walk Through:** If you select this menu item, you will be given a list
of serial ports. Select the port where your modem is attached. The ports
included are those used by a Linmodem, but this script does not install
or setup these modems. If you know what COM device it was under Windows,
you can select the listed equivalent. Otherwise, you can try the
autodetect or you might have to do some experimenting. The best course
is to start at ttyS0 and work your way down the list.

Next you will be asked for the user name and password for your ISP's
connection for PPP dialers such as X-ISP. You can skip this if you are
using GkDial or KPPP, which you will have to configure yourself.


FIREWALL
~~~~~~~~

**Purpose:** Sets up basic network protection and Internet sharing.

**Command Line:** /sbin/vfirewall

**Walk Through:** This option calls a submenu of options to setup a
basic firewall/gateway. The options are:

-  **New**
   This option creates a script */etc/rc.d/rc.firewall* and makes it
   executable so it will start every time the system is started.
-  **Open**
   Allows the user to open some common ports on the system for others
   to access. The user can also modify the rc.firewall script.
-  **Gateway**
   This script sets up a simple gateway, using ip masquerading (NAT),
   with a firewall using iptables. The user is asked to identify the
   interface that is connected to the local area network for sharing.
   This interface will need to be set up using the INET option of the
   NETWORK submenu
-  **Start**
   Starts the firewall.
-  **Stop**
   Stops the firewall.
-  **Enable**
   Enables the firewall to be started each time the system starts.
-  **Disable**
   Disables the automatic starting of the firewall.

.. warning:: 
    This firewall script is able to handle simple
    filtering (open/close the host port) and simple masquerading (become an
    Internet gateway).
    However, this script is relatively basic, so it is recommended that you
    use more advanced firewall tools (like guarddog/guidedog, included in
    SOHO) to create more advanced rules.
    For these "advanced" rules to take effect, you should use VASM to **set
    the firewall active** but **not** to configure it, since you will do
    that through guarddog/guidedog.


HARDWARE
--------

**Purpose:** Setup keyboard, mouse, CD-ROM drives, sound, etc.

**Command Line:** /sbin/vhwmenu

|image16|

*HARDWARE menu in Graphical Mode.*


HWCONF
~~~~~~

**Purpose:** Autodetect hardware devices on your system.

**Command line:** /sbin/vhwconf

.. note:: 
    The autodetection routine will start as soon as you select
    this option. Be sure that you want re-detect your hardware before
    selecting this.

**Walk Through:** Must be run from console mode. If used under X you
will receive the message telling you so.

When you select this option the system will immediately start to probe
the system's components. You will see windows showing detected devices
as they are found and configured. Since this is an AUTO-Probe you don't
have the means to configure the detected devices - the system will do
that for you. This mostly deals with the detection of the hardware
itself and not so much with the fine-tuning. For example, it will detect
and activate your ethernet cards, but if you want to enter you network
configuration you have to resort to the "Network Menu".

**Screenshot:**

.. figure:: ../_static/images/img_vasm/vhwconf.png
   :align: center
   :alt: 


HWSET
~~~~~

**Purpose:** This menu allows you to enable or disable the
initialization of hardware components in your system.

**Command line:** /sbin/vhwset

**Walk Through:** When selecting this entry, a list will appear, showing
the hardware you can initialize at boot time. This means that if you
plan to use a Parallel port printer you need to set "parallel" to be
initialized at boot time, but if you don´t plan to, you can disable it
and free some system resources.The available options are:

-  **PNP**
   ISA Plug and Play. This should be enabled for a modern PC.
-  **Hotplug**
   USB/PCI hotplug devices (pendrive, digital camera, etc).
-  **PCMCIA**
   PCMCIA cards, common on laptops (network, modem, etc.).
-  **APM**
   Advanced Power Management, useful for Laptops.
-  **Serial**
   Serial port and modem.
-  **Parallel**
   Parallel port and printer.
-  **Wireless**
   Networking using radio or infrared.
-  **ALSA**
   Advanced Linux Sound System.
-  **udev**
   Dynamic /dev naming for hotplug on kernel 2.6.
-  **tmpfs**
   On memory /tmp. Recommended if RAM>=196MB + SWAP>=256MB.


ALSACONF
~~~~~~~~

**Purpose:** Setup ALSA (Advanced Linux Sound Architecture).

**Command line:** /sbin/valsaconf

**Walk Through:** Must be run from console mode. If used under X you
will receive the message telling you so.

When you run ALSACONF, a dialog appears asking you to close any open
application related to sound. When you press "OK" the system will try to
autodetect any present sound device and you will see a progress bar
indicating the status of the detection.

You will then be presented with the detected cards and asked to select
the one to use or if you want to probe the system for older, non-PnP,
ISA cards.

If ALSA detected any onboard or PCI sound device and you chose that one
then when you press "OK" the driver will be loaded and the device will
be configured for activation at boot time. Press "Ok"

To accept the confirmation dialog and you will be taken back to the
"Hardware Menu".

If you choose to probe the system for legacy sound cards you will
receive a warning stating that the system may become unstable; press
"Yes" to proceed.

A dialog appears with a list of the available modules (drivers) to
probe. While the list may seem a bit short, it actually covers a much
wider range of sound cards which work with similar drivers.

Uncheck any particular module you don't want to be probed and press
"Ok". You will be asked if the system should probe for possible IRQ and
DMA combinations. Since doing so takes more time, you could skip it for
now to see if the card is detected, and if it is not, then re-run
alsaconf enabling this option instead. You will finally see the results
of the probing and a request to choose the card to be used by the
system.


CDSET
~~~~~

**Purpose:** Allows you to configure the location of your CD-ROM and
CD-Writer drives.

**Command line:** /sbin/vcdset

**Walk Through:** When a CD-Rom is detected, you will be prompted to
accept the creation of a symbolic link (/mnt/cdrom) pointing to it.

When a CD-Writer is detected, either after, before or instead of a
reader unit, you will be asked if you want to allow access to it by
ordinary users, if you select "Yes", they will be able to use programs
like XCDRoast, otherwise, only root will have access to it.

Using CDSET results in some changes to the LILO bootloader, so you will
need to reboot the system for the changes to take effect.


MOUSESET
~~~~~~~~

**Purpose:** Configure your pointing devices type and protocol.

**Command line:** /sbin/vmouseset

**Walk Through:** When you select this entry you are presented with a
list of possible pointing devices, from standard mice to graphic
tablets, plus an autodetection option.

After selecting your device type, you are asked some more details like
how many buttons does it have and, for serial devices, what port it is
attached to. After entering this information you are taken back to the
Hardware Menu.

The mouse configuration takes effect after you start or re-restart the X
Server, thus, to avoid finding yourself with a non-working mouse if you
select an incorrect configuration, it is highly recommended that you
**switch to text mode** to use this feature (see chapter 5).


KEYMAPSET
~~~~~~~~~

**Purpose:** Change the map layout used by your keyboard.

**Command line:** /sbin/vkmapset

**Walk Through:** Select the one that matches it and press "OK" test the
selection by typing characters such as @ & #, erase them, and type 1 (by
itself) to accept the layout. Type 2 (by itself), or "Cancel", to go
back to the maps selection and choose a different one.


DATESET
~~~~~~~

**Purpose:** Set date and time of the system.

**Command line:** /sbin/vdateset

**Walk Through:** The use of this option is rather self-explanatory.
Select the date by clicking it, select a different month or year using
the arrows next to them, and then press "Ok". Do the same for the time,
and then press "Next" to commit the changes to the system.


ZONESET
~~~~~~~

**Purpose:** Changes settings to reflect if your clock is set to local
or universal time zone.

**Command Line:** /sbin/vzoneset

**Walk Through:** When you choose this option you will be asked if the
clock in your machine is set to local or UTC time. Choose no if local
and yes if UTC. Most likely it is set to local time. You will then be
given a list of timezones, select the best match and press "Next" to
activate the change.


FILESYSTEM
----------

Perform disk partitioning and formatting, setup drives and partitions to
be mounted, LiLo bootloader and system configuration backups.

**Command Line:** /sbin/vfsmenu

|image17|

*FILESYSTEM menu in Graphical Mode.*


FDISK
~~~~~

**Purpose:** This option launches *cfdisk*, a text mode application
capable of editing your hard disk partitions.

**Command line:** /sbin/vfdisk

The use of this program is beyond the scope of this guide, but we advise
you to use it with care, and to remember that you must select the
"Write" option for any changes to be committed to the partition map
before you exit the application.


FORMAT
~~~~~~

**Purpose:** Formats partitions, clearing all the data in them, while
creating a new filesystem.

**Command line:** /sbin/vformat

**Walk Through:** By launching this option, you are presented with a
list of the partitions on your system that can be formatted. Only
**unmounted** partitions can be formatted, so make sure the one you
intend to format meets this condition or it will not be presented in the
list.

To format a partition select it from the list and press OK, then follow
the instructions.

.. warning::
    Be careful when you select the partition to format, once it
    is done, all of the information contained in it will be erased.


MOUNT
~~~~~

**Purpose:** Setup additional partitions to be mounted at boot time.

**Command line:** /sbin/vmount

**Walk Through:** This tool is an easy method to configure additional
local disks or partitions to be mounted in your system. Select the
option and you can ADD or REMOVE mount points.

-  **Add**
   If you select "Add" you will be presented with a list of the local
   filesystems which are available and which are not already present in
   */etc/fstab* (the file where mount point configurations are stored).
   Select the one you want to configure and you will be asked where you
   want it to be mounted. Then you will see the default options it will
   be mounted with. If you know what you are doing, you can modify these
   options, otherwise accept the defaults and press "Ok".
   The filesystem will not be automatically mounted, you will have to
   mount it manually by either command line, Kwikdisk, or utilities such
   as gkrellm.
   If you mount it by command line, and assuming you created a mount
   point in /mnt/win, you could use this command (as root):
   *mount /mnt/win*
   You could also use:
   *mount -a*
   this will mount *all* of the mount points available in the
   configuration file, /etc/fstab.
   
.. note::
    Keep in mind that the default settings make the filesystem
    mountable ONLY by the root user and not by ordinary users. If you
    want to make it mountable by users you could append "users," to the
    default options that are presented during the process.

-  **Del**
   If you select "Del" you will be deleting a "mount point". This tells
   the system not to access some particular filesystem (disk, partition,
   etc,). You are not actually deleting any contents of that filesystem.
   So, if you choose to Delete a mount point, you will see a list of
   the devices configured to be mountable (not necessarily
   auto-mounted). Simply select those you want to remove and press "Ok".
   Their entries will be removed from the configuration files and
   nothing will be modified in the filesystem itself.

 .. note::
    It is highly recommended that you unmount the filesystem
    BEFORE you delete it's mount point. If you do not unmount the
    filesystem beforehand it will not be unmounted automatically so you
    will have to do it either by command line, Kwikdisk, gkrellm or by
    rebooting the system.


LILO
~~~~

**Purpose:** This option helps you build the */etc/lilo.conf*
configuration file so that you can install LILO to your system. The
Linux Loader, or LILO, is a popular booter in use on many Linux systems.
It is quite configurable and can easily be used to boot other operating
systems such as Windows.

**Command Line:** /sbin/vliloconfig

**Walk Through:** When selecting this option, you will be given the
choice to use simple or expert mode.

If this is your first time setting up LILO, you should pick the "simple"
option. If you are familiar with LILO and Linux, the "expert mode" may
provide you additional levels of control, which could lead to faster
configuration.

-  **Simple Setup** **
   **
   If kernel frame buffer support is compiled into your kernel,
   **liloconfig** will ask which video resolution you would like to use.
   This is the resolution that is also used by the XFree86 frame buffer
   server. If you do not want the console to run in a special video
   mode, selecting normal will keep the standard 80x25 text mode in use.

   The next part of the LILO configuration is selecting where you want
   it installed. This is probably the most important step. The list
   below explains the installation places:

   - Root
     This option installs LILO to the beginning of your Linux root
     partition. This is the safest option if you have other operating
     systems on your computer. It ensures that any other bootloaders are
     not overwritten. The disadvantage is that LILO will only load from
     here if your Linux drive is the first drive on your system.
   - Floppy
     This method is even safer than the previous one. It creates a boot
     floppy that you can use to boot your Linux system. This keeps the
     bootloader off the hard disk entirely, so you only boot this floppy
     when you want to use VectorLinux. However, this boot method is the
     slowest.
   - MBR
     You will want to use this method if VectorLinux is the only
     operating system on your computer, or if you will be using LILO to
     choose between multiple operating systems on your computer. This is
     the most common method.

.. warning:: 
    This option will overwrite any other bootloader you
    have in the MBR.

    After selecting the installation location, **liloconfig** will write
    the configuration file and install LILO.

-  **Expert**
   If you select the "expert" mode you will receive a special menu.
   This menu allows you to tweak the */etc/lilo.conf* file, add other
   operating systems to your boot menu, and set LILO to pass special
   kernel parameters at boot time.


BACKUPSYS
~~~~~~~~~

**Purpose:** Allows you to backup important system files.

**Command line:** /sbin/vbackupsys

**Walk Through:** This utility allows you to keep multiple backups (up
to 9). It is also designed to backup system configuration files, which
most of the time are small text files. There are three available
options:

-  **Cron**
   Schedule regular backups. Selecting this option means that the cron
   daemon will be configured to start at boot time and perform the
   backup on a daily basis.
-  **Backup**
   Perform a one time backup. You can choose to perform an update of an
   existing backup, where only the files that were changed are added to
   the compressed archive (something known as an incremental backup). Or
   you can perform a full backup where all of the designated files are
   stored in a new, additional compressed archive.
   After you select and accept the backup method, the process will
   begin and, when done, you will be presented with a report of the
   results.
-  **Restore**
   Restore files from backup archives. After selecting this option you
   will be presented with a list of the available backup archives.
   Select the one you wish to restore based on its date.
   Once you selected the archive, a dialog window will appear, where
   you can individually select which files to restore.
   **Warning:** when you restore a file, it will overwrite the one
   currently in use. If you want to keep both files you will have to
   manually copy the current file to a safe place and only then restore
   the old one from the archive.


.. |image1| image:: ../_static/images/img_vasm/vasm_gui.png
.. |image2| image:: ../_static/images/img_vasm/vasm_tui.png
.. |image3| image:: ../_static/images/img_vasm/vasmcc1.jpg
.. |image4| image:: ../_static/images/img_vasm/vasmcc2.jpg
.. |image5| image:: ../_static/images/img_vasm/vasmcc3.jpg
.. |image6| image:: ../_static/images/img_vasm/vasmcc4.jpg
.. |image7| image:: ../_static/images/img_vasm/vasmcc5.jpg
.. |image8| image:: ../_static/images/img_vasm/vasmcc6.jpg
.. |image9| image:: ../_static/images/img_vasm/vasmcc7.jpg
.. |image10| image:: ../_static/images/img_vasm/vasm_users.png
.. |image11| image:: ../_static/images/img_vasm/vasm4.jpg
.. |image12| image:: ../_static/images/img_vasm/vasm5.jpg
.. |image13| image:: ../_static/images/img_vasm/vhwset.png
.. |image14| image:: ../_static/images/img_vasm/vsrvset02.png
.. |image15| image:: ../_static/images/img_vasm/vasm6.jpg
.. |image16| image:: ../_static/images/img_vasm/vasm7.jpg
.. |image17| image:: ../_static/images/img_vasm/vasm8.jpg
