
#############
Vlocity Linux
#############


What is Vlocity?
================

.. versionadded:: 7.0
Vlocity is a robust x64_64 desktop-ready GNU/Linux system packed with the best
breed of applications for your every day dasks.


Key Features
============

* A choice of a very intuitive graphical installer, or a traditional text-based
  installer. Both do great on hardware detection and install time is only about
  12-15 minutes on modern hardware.
* A well customized gnome3 for your desktop environment. A great desktop offering
  everything you demand on a day-to-day basis.
* Vasm and VASMCC for system tuning and administration
* User friendly GUI package manager (gslapt) and it's command-line counterpart
  slapt-get make installing and upgrading software easy.


Hardware Requirements
=====================

It is recommended that your system meets or exeeds these requirements:

* Intel/AMD x86_64 processor.
* A minimum of 4GB of hard disk space for the entire system.
* 756 MB of RAM.
* Video card and monitor.
* CD-ROM drive.
* Standard mouse, keyboard, sound card, etc.

